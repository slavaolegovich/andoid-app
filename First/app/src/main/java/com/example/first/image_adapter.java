package com.example.first;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;


public class image_adapter extends ArrayAdapter {
    private Integer[] imageid;
    private Activity context;

     public image_adapter(Activity context,Integer []  imageid) {
        super(context, R.layout.img_adapter);
        this.context=context;
        this.imageid=imageid;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

            View r=convertView;
            ViewHolderImage viewHolderImage=null;
            if(r==null)
            {
                LayoutInflater layoutInflater;
                layoutInflater = context.getLayoutInflater();
                r=layoutInflater.inflate(R.layout.img_adapter,null,true);
                viewHolderImage=new ViewHolderImage(r);
                r.setTag(viewHolderImage);
            }
            else{
                viewHolderImage=(ViewHolderImage) r.getTag();

            }
            viewHolderImage.images.setImageResource(imageid[position]);
            return r;
        }


    class ViewHolderImage
    {

        ImageView images;

        ViewHolderImage(View v)
        {

            images=v.findViewById(R.id.testIMG);
        }
    }

}
