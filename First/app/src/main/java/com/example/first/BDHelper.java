package com.example.first;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDHelper extends SQLiteOpenHelper {

   public static final int DATABASE_VERSION =1;
   public static final String DATABASE_NAME= "listDrinksBDtest.db";
   public static final String TABLE_DRINKS= "drinks";

   public static final String KEY_ID="_id";
   public static final String KEY_NAME="name";
   public static final String  KEY_ALCOGOL="alcohol";
   public static final String  KEY_ML="ml";
   public static final String  KEY_IMAGE="imageT";


    public BDHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table "+ TABLE_DRINKS+"("+KEY_ID
                +" integer primary key,"+KEY_NAME+" text,"+KEY_ALCOGOL+" text," + KEY_ML+" text,"+KEY_IMAGE+" text"+")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists "+TABLE_DRINKS);
        onCreate(sqLiteDatabase);

    }
}
