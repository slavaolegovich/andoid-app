package com.example.first;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button button1,button2,button3,button4,button5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1=(Button)findViewById(R.id.Button1);
        button2=(Button)findViewById(R.id.Button2);
        button3=(Button)findViewById(R.id.Button3);
        button4=(Button)findViewById(R.id.Button4);
        //button5=(Button)findViewById(R.id.Button5);
        listenerMain();
    }
    public void listenerMain(){

        button1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent nextPage=new Intent(".SecondActivity");
                        SecondActivity.user.setCondition(0.6f);
                        startActivity(nextPage);
                    }
                }
        );
        button2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent nextPage=new Intent(".SecondActivity");
                        SecondActivity.user.setCondition(1.1f);
                        startActivity(nextPage);
                    }
                }
        );
        button3.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent nextPage=new Intent(".SecondActivity");
                        SecondActivity.user.setCondition(1.6f);
                        startActivity(nextPage);
                    }
                }
        );
        button4.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent nextPage=new Intent(".SecondActivity");
                        SecondActivity.user.setCondition(2.3f);
                        startActivity(nextPage);
                    }
                }
        );
        /*button5.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent nextPage=new Intent(".SecondActivity");
                        SecondActivity.user.setCondition(3f);
                        startActivity(nextPage);
                    }
                }
        );*/
    }
}





