package com.example.first;

public class Drink {
    private String name;
    private float degree;
    private int ml;
    private int id;

    Drink(String name,float degree,int ml,int id){
        this.name=name;
        this.degree=degree;
        this.ml=ml;
        this.id=id;
    }
    Drink(){
        this.name="None";
        this.degree=0f;
    }
    Drink(Drink temp)
    {
        this.name=temp.name;
        this.degree=temp.degree;
        this.ml=temp.ml;
        this.id=temp.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getDegree() {
        return degree;
    }

    public int getMl() {
        return ml;
    }

    public void setDegree(float degree) {
        this.degree = degree;
    }

    public void setMl(int ml) {
        this.ml = ml;
    }

    public void setName(String name) {
        this.name = name;
    }
}
