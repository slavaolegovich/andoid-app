package com.example.first;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class drink_adapter extends ArrayAdapter<String> {
    private String[] name;
    private float [] degree;
    private int [] ml;
    private Integer[] imageid;
    private Activity context;
    public drink_adapter(Activity context,String[] name,float [] degree,int [] ml,Integer[] imageid) {
        super(context, R.layout.list_view_image,name);
        this.context=context;
        this.name=name;
        this.degree=degree;
        this.ml=ml;
        this.imageid=imageid;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View r=convertView;
        ViewHolder viewHolder=null;
        if(r==null)
        {
            LayoutInflater layoutInflater=context.getLayoutInflater();
            r=layoutInflater.inflate(R.layout.list_view_image,null,true);
            viewHolder=new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else{
            viewHolder=(ViewHolder) r.getTag();

        }
        viewHolder.images.setImageResource(imageid[position]);
        viewHolder.tvw1.setText(name[position]);
        viewHolder.tvw2.setText(String.valueOf(degree[position]));
        viewHolder.tvw3.setText(String.valueOf(ml[position]));
        return r;
    }
    class ViewHolder
    {
        TextView tvw1;
        TextView tvw2;
        TextView tvw3;
        ImageView images;

        ViewHolder(View v)
        {
            tvw1=v.findViewById(R.id.twName);
            tvw2=v.findViewById(R.id.twDefree);
            tvw3=v.findViewById(R.id.twMl);
            images=v.findViewById(R.id.iwImage);
        }
    }
}
