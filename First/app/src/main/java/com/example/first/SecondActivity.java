package com.example.first;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;



import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    public static Person user =new Person();
    private String[] age = new String[82];
    private Spinner spinnerAge;
    private ImageView imageSex;
    private int idSeekBarH;
    private SeekBar seekBarWeight,seekBarHeight;;
    private TextView textName,textHeight,textWeight;
    private Button start;
    private RadioGroup rGroupCondition,rGroupSex;
    private Switch sSatiety;
    private Intent nextPage3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        spinnerAge = (Spinner) findViewById(R.id.spinnerYears);
        ArrayAdapter<String> adapterAge = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item, age);
        spinnerAge.setAdapter(adapterAge);
        for (int i = 0; i < age.length; i++) {
            age[i] = String.valueOf(i + 18);
        }
        seekBarHeight=(SeekBar)findViewById(R.id.seekBarHeight);
        seekBarWeight=(SeekBar)findViewById(R.id.seekBarWeight);
        seekBarHeight.setOnSeekBarChangeListener(this);
        seekBarWeight.setOnSeekBarChangeListener(this);
        //imageSex=(ImageView)findViewById(R.id.ImageSex);
        textHeight = (TextView) findViewById(R.id.TextHeight);
        textWeight = (TextView) findViewById(R.id.TextWeight);
        //rGroupSex=(RadioGroup)findViewById(R.id.rGroupSex);
        //textName = (TextView) findViewById(R.id.textName);
        start = (Button) findViewById(R.id.buttonStart);
        //sSatiety=(Switch)findViewById(R.id.switchSatiety);

        listener();

    }

    public void listener() {

        start.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //user.setName(textName.getText().toString());
                        int a =spinnerAge.getFirstVisiblePosition();
                        user.setAge(Integer.parseInt(age[a]));
                        user.setWeight(seekBarWeight.getProgress()+35);
                        user.setHeight(seekBarHeight.getProgress()+100);
                        nextPage3=new Intent(".ThirdActivity");
                        startActivity(nextPage3);


                    }
                }
        );
        /*rGroupSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rButtonM:
                        imageSex.setImageResource(R.drawable.beer);
                        user.setSex(0.7f);
                        break;
                    case R.id.rButtonF:
                        imageSex.setImageResource(R.drawable.cocktail);
                        user.setSex(0.6f);
                        break;
                    default:
                        break;
                }
            }
        });*/
        sSatiety.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                user.setSatiety(isChecked);
            }
        });
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

        if(seekBar.getId()==seekBarHeight.getId()) {
            textHeight.setText(String.valueOf(seekBar.getProgress()+140));
        }
        else textWeight.setText(String.valueOf(seekBar.getProgress()+40));


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {


    }
}

