package com.example.first;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import static com.example.first.SecondActivity.user;

public class ThirdActivity extends AppCompatActivity {
    private ListView list;
    private ProgressBar progressDrinked;
    private Drink[] arrayDrinks;
    private Drink minDrink;
    private Integer [] imageArray={R.drawable.beer,R.drawable.cocktail,R.drawable.cocktail1,R.drawable.cocktail2,
    R.drawable.cocktail3,R.drawable.beer_bottle1,R.drawable.glass_and_bottle,R.drawable.m_glass,R.drawable.soda_drink,R.drawable.vine_glass};
    private TextView test;
    private ImageView testimg;
    private float drunk;
    private Timer timer;
    private BDHelper dbhelper;
    private Button bAddDrink, bOkAdd, bCancelAdd,choiseNextImage,choisePreviousImage;;
    private ConstraintLayout layoutAdd;
    private EditText nameAddDrink, degreeAddDrink, mlAddDrink;
    private int nextI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        progressDrinked = (ProgressBar) findViewById(R.id.ProgressDrinked);
        list = (ListView) findViewById(R.id.ListDrinks);
        minDrink = new Drink();
        minDrink.setDegree(99f);
        bAddDrink = (Button) findViewById(R.id.ButtonAddDrink);
        bOkAdd = (Button) findViewById(R.id.buttonOK);
        bCancelAdd = (Button) findViewById(R.id.buttonCancelAdd);
        layoutAdd = (ConstraintLayout) findViewById(R.id.LayoutAdd);
        nameAddDrink = (EditText) findViewById(R.id.eTextName);
        degreeAddDrink = (EditText) findViewById(R.id.eTextDegree);
        mlAddDrink = (EditText) findViewById(R.id.eTextML);
        layoutAdd.setVisibility(View.GONE);
        choiseNextImage=(Button) findViewById(R.id.btnNextChoise);
        choisePreviousImage=(Button)findViewById(R.id.btnPreviousChoise);
        timer = new Timer();
        testimg=(ImageView)findViewById(R.id.imgChoise) ;
        dbhelper = new BDHelper(this);
        nextI=0;
        listener();
    }

    private void listener() {
        updateList();

        timer.schedule(new UpdateTimeTask(), 0, 1000);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {

                if (!drinkUp(arrayDrinks[position].getMl() * (arrayDrinks[position].getDegree() / 100)))
                {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            /*user.getName() + */" тебе пока что хватит!!!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

        });
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View itemClicked,
                                           int position, long id) {
                final SQLiteDatabase database = dbhelper.getWritableDatabase();
                database.delete(BDHelper.TABLE_DRINKS, BDHelper.KEY_ID + "=" + arrayDrinks[position].getId(), null);
                updateList();
                database.close();
                return true;
            }
        });


        bAddDrink.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bAddDrink.setVisibility(View.GONE);
                        layoutAdd.setVisibility(View.VISIBLE);


                    }
                }
        );

        bOkAdd.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final SQLiteDatabase database = dbhelper.getWritableDatabase();
                        final ContentValues contentValues = new ContentValues();
                        contentValues.put(BDHelper.KEY_NAME, nameAddDrink.getText().toString());
                        contentValues.put(BDHelper.KEY_ALCOGOL, degreeAddDrink.getText().toString());
                        contentValues.put(BDHelper.KEY_ML, mlAddDrink.getText().toString());
                        contentValues.put(BDHelper.KEY_IMAGE,String.valueOf(nextI));
                        database.insert(BDHelper.TABLE_DRINKS, null, contentValues);
                        bAddDrink.setVisibility(View.VISIBLE);
                        layoutAdd.setVisibility(View.GONE);
                        updateList();
                        database.close();

                    }
                }
        );

        bCancelAdd.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bAddDrink.setVisibility(View.VISIBLE);
                        layoutAdd.setVisibility(View.GONE);
                    }
                }
        );
        choiseNextImage.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(nextI<(imageArray.length-1)){
                            nextI++;
                            testimg.setImageResource(imageArray[nextI]);
                        }

                    }
                }
        );
        choisePreviousImage.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(nextI>0){
                            nextI--;
                            testimg.setImageResource(imageArray[nextI]);
                        }

                    }
                }
        );

    }


    private Drink[] readDB() {
        final SQLiteDatabase database = dbhelper.getWritableDatabase();
        Cursor cursor = database.query(BDHelper.TABLE_DRINKS, null, null, null, null, null, null);
        Drink[] readArrayDrink = new Drink[cursor.getCount()];
        String [] name=new String[cursor.getCount()];
        float[] degree=new float[cursor.getCount()];
        int[] ml=new int[cursor.getCount()];
        Integer[]imageid=new Integer[cursor.getCount()] ;

        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex((BDHelper.KEY_ID));
            int nameIndex = cursor.getColumnIndex(BDHelper.KEY_NAME);
            int degreeIndex = cursor.getColumnIndex(BDHelper.KEY_ALCOGOL);
            int mlIndex = cursor.getColumnIndex(BDHelper.KEY_ML);
            int ImageIndex=cursor.getColumnIndex(BDHelper.KEY_IMAGE);
            Drink temp = new Drink();
            int i = 0;
            do {
                name[i]=cursor.getString(nameIndex);
                degree[i]=cursor.getFloat(degreeIndex);
                ml[i]=cursor.getInt(mlIndex);
               // ml[i]=cursor.getInt(ImageIndex);
                //imageid[i]=R.drawable.soda_drink;
                imageid[i]=imageArray[cursor.getInt(ImageIndex)];
                temp.setId(cursor.getInt(idIndex));
                temp.setName(cursor.getString(nameIndex));
                temp.setDegree(cursor.getFloat(degreeIndex));
                temp.setMl(cursor.getInt(mlIndex));
                readArrayDrink[i] = new Drink(temp);
                if (minDrink.getDegree() > readArrayDrink[i].getDegree())
                    minDrink = readArrayDrink[i];
                i++;

            } while (cursor.moveToNext());
        }
        cursor.close();
        database.close();
        drink_adapter drinkAdapter=new drink_adapter(this,name,degree,ml,imageid);
        list.setAdapter(drinkAdapter);
        return readArrayDrink;
    }

  //  private List<String> initListDrinks(Drink[] drinks) {
   //     List<String> listDrink = new ArrayList<String>();

   //     for (Drink d : drinks) {
   //         String tempText;
   //         tempText=d.getName();
  //          for(int i=(0+d.getName().length());i<18;i++) {tempText+=" ";}
   //         tempText+= d.getDegree() + "%";
  //          for(int i=(0+(String.valueOf(d.getDegree())).length());i<10;i++){tempText+=" ";}
  //          tempText+=d.getMl() + "ml";
            //listDrink.add(d.getName() + ": " + d.getDegree() + "%  :" + d.getMl() + "ml");
  //          listDrink.add(tempText);
  //      }
  //      return listDrink;
  //  }

    private boolean drinkUp(float addDrink) {
        if (((user.getStateEthanol(drunk + addDrink) / user.condition) * 100) >= 100) {
            return false;
        } else  {
            drunk += addDrink;
            progressDrinked.setProgress(Math.round((user.getStateEthanol(drunk) / user.condition) * 100));
            return true;
        }

    }


    private void updateList(){
        arrayDrinks=readDB();
    }

    class UpdateTimeTask extends TimerTask {
        public void run() {
            if(drunk>0)drunk-=0.5;
            progressDrinked.setProgress(Math.round((user.getStateEthanol(drunk) / user.condition) * 100));
        }

    }
}



